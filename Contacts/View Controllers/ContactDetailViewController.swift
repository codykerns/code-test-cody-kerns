//
//  ContactDetailViewController.swift
//  Code Test Cody Kerns
//
//  Created by Cody Kerns on 12/17/18.
//  Copyright © 2018 Cody Kerns. All rights reserved.
//

import UIKit
import EventKit

class ContactDetailViewController: UITableViewController {

    //Defaults
    var currentContact: Person?
    var isNewContact: Bool = false
    var isEditingContact: Bool = false

    //Easy dynamic var for tableView setup
    var showEditOptions: Bool {
        return isNewContact || isEditingContact
    }
    
    //Date picker
    let datePickerTextField = UITextField()
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.navigationItem.largeTitleDisplayMode = .never
        
        self.tableView.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.00)
        
        datePicker.datePickerMode = .date
        
        if isNewContact {
            currentContact = Person(context: ContactsManager.shared.context)
        }
        
        if isEditingContact {
            let deleteButton = UIBarButtonItem(title: "Delete Contact", style: .plain, target: self, action: #selector(deleteContact))
            deleteButton.tintColor = .red
            self.toolbarItems = [
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                deleteButton,
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            ]
            self.navigationController?.setToolbarHidden(false, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
    }
    
    func updateUI() {
        if showEditOptions {
            
            if isNewContact {
                self.title = "New Contact"
            } else if isEditingContact {
                self.title = "Edit Contact"
            }
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelContactCreation))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveContact))
            
        } else {
            self.title = "Contact Details"
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editContact))
        }
        
        self.tableView.reloadData()
    }
    
    @objc func deleteContact() {
        let confirmation = UIAlertController(title: "Delete this contact?", message: "This cannot be undone.", preferredStyle: .actionSheet)
        confirmation.popoverPresentationController?.barButtonItem = self.navigationItem.leftBarButtonItem
        confirmation.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        confirmation.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {
            (action) in
            
            let context = ContactsManager.shared.context
            if let contact = self.currentContact {
                context.delete(contact)
                try? context.save()
            }
            if let presenter = self.presentingViewController as? UINavigationController {
                presenter.popViewController(animated: true)
            }
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(confirmation, animated: true, completion: nil)
        UINotificationFeedbackGenerator().notificationOccurred(.warning)

    }
    
    @objc func cancelContactCreation() {
        let context = ContactsManager.shared.context

        if context.hasChanges == true {
            let confirmation = UIAlertController(title: "Cancel without saving?", message: "Your changes will be discarded.", preferredStyle: .actionSheet)
            confirmation.popoverPresentationController?.barButtonItem = self.navigationItem.leftBarButtonItem
            confirmation.addAction(UIAlertAction(title: "Keep Editing", style: .cancel, handler: nil))
            confirmation.addAction(UIAlertAction(title: "Discard Changes", style: .destructive, handler: {
                (action) in
                context.rollback()
                
                self.dismissDetails()
                
            }))
            self.present(confirmation, animated: true, completion: nil)
            UINotificationFeedbackGenerator().notificationOccurred(.warning)

        } else {
            dismissDetails()
        }
    }
    
    @objc func dismissDetails() {
        if let _ = self.presentingViewController, isNewContact || isEditingContact {
            self.dismiss(animated: true, completion: nil)
        } else if isEditingContact {
            self.isEditingContact = false
            self.tableView.reloadData()
        }
    }
    
    @objc func saveContact() {
        let context = ContactsManager.shared.context
        
        guard let contact = self.currentContact else {
            return
        }
        
        if contact.firstName == nil
            || contact.lastName == nil
            || contact.dateOfBirth == nil
            || contact.email?.count == 0
            || contact.phone?.count == 0 {
            
            self.showStatusMessage(title: "Incomplete Details", message:  "You must enter a first name, last name, date of birth, as well as at least one phone number and email address.")
            
            return
        }
        
        do {
            try context.save()
        } catch let err {
            print(err)
        }
        
        dismissDetails()
    }
    
    @objc func editContact() {
        performSegue(withIdentifier: "EditContact", sender: nil)

    }

    @objc func toggleFavorite(sender: UITapGestureRecognizer? = nil) {
        self.currentContact?.isFavorite = !(self.currentContact?.isFavorite ?? false)
        try? ContactsManager.shared.context.save()
        
        if let favoriteLabel = sender?.view as? UILabel {
            if self.currentContact?.isFavorite == true {
                favoriteLabel.text = "★"
                favoriteLabel.textColor = self.navigationController?.navigationBar.tintColor
                
                favoriteLabel.transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
                
                UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.55, initialSpringVelocity: 0.4, options: [.curveLinear], animations: {
                    favoriteLabel.transform = .identity
                }, completion: nil)
                
                UIImpactFeedbackGenerator().impactOccurred()
                
            } else {
                favoriteLabel.text = "☆"
                favoriteLabel.textColor = .lightGray
            }
        }
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return nil
        } else if section == 1 {
            return "Date of Birth"
        } else if section == 2 {
            return "Phone"
        } else if section == 3 {
            return "Email"
        } else if section == 4 {
            return "Address"
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        //Although there should always be a phone and email associated, footer message just in case
        if section == 2, currentContact?.phone?.count == 0, !showEditOptions {
            return "No phone number associated with this contact."
        } else if section == 3, currentContact?.email?.count == 0, !showEditOptions {
            return "No email address associated with this contact."
        } else if section == 4, currentContact?.address?.count == 0, !showEditOptions {
            return "No address associated with this contact."
        }
        
        return nil
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return showEditOptions ? 2 : 1
        case 1:
            return (currentContact?.dateOfBirth != nil && showEditOptions == false) ? 2 : 1
        case 2:
            return (currentContact?.phone?.count ?? 0) + (showEditOptions ? 1 : 0)
        case 3:
            return (currentContact?.email?.count ?? 0) + (showEditOptions ? 1 : 0)
        case 4:
            return (currentContact?.address?.count ?? 0) + (showEditOptions ? 1 : 0)
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.textLabel?.text = nil
        cell.textLabel?.textColor = .black
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17.0)
        cell.selectionStyle = .default
        cell.accessoryView = nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObjectCell", for: indexPath)
        
        let actionColor = self.navigationController?.navigationBar.tintColor
        
        //check if row is last in its section
        let isLastRow = Bool(indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1)
        
        if indexPath.section == 0 { //Name section
            if showEditOptions {
                cell.textLabel?.textColor = actionColor
                
                if indexPath.row == 0 {
                    cell.textLabel?.text = currentContact?.firstName ?? "First Name"
                } else if indexPath.row == 1 {
                    cell.textLabel?.text = currentContact?.lastName ?? "Last Name"
                }
            } else {
                cell.textLabel?.font = UIFont.systemFont(ofSize: 20.0, weight: .regular)
                cell.selectionStyle = .none

                if let contact = self.currentContact {
                    let attributedName = NSMutableAttributedString(string: contact.fullName)
                    
                    if let lastName = contact.lastName {
                        attributedName.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 20.0, weight: .bold), range: NSMakeRange(contact.fullName.count - lastName.count, lastName.count))
                    }
                    
                    cell.textLabel?.attributedText = attributedName
                    
                    let favoriteLabel = UILabel()
                    favoriteLabel.font = UIFont.systemFont(ofSize: 28.0)
                    if contact.isFavorite == true {
                        favoriteLabel.text = "★"
                        favoriteLabel.textColor = self.navigationController?.navigationBar.tintColor
                    } else {
                        favoriteLabel.text = "☆"
                        favoriteLabel.textColor = .lightGray
                    }
                    favoriteLabel.sizeToFit()
                    favoriteLabel.isUserInteractionEnabled = true
                    favoriteLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toggleFavorite(sender:))))
                    cell.accessoryView = favoriteLabel
                }
            }
        } else if indexPath.section == 1 { //Birthday section
            if indexPath.row == 0 {
                if let birthday = currentContact?.dateOfBirth {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MMMM dd, YYYY"
                    cell.textLabel?.text = formatter.string(from: birthday)
                    cell.selectionStyle = .none
                } else {
                    if showEditOptions {
                        cell.textLabel?.text = "Select a Date"
                        cell.textLabel?.textColor = actionColor

                    } else {
                        cell.textLabel?.text = "Not set"
                    }
                }
            } else if indexPath.row == 1 {
                cell.textLabel?.text = "Add to calendar"
                cell.textLabel?.textColor = actionColor
            }
        } else if indexPath.section == 2 { //Phone section
            if isLastRow, showEditOptions {
                cell.textLabel?.text = "Add Phone Number"
                cell.textLabel?.textColor = actionColor
            } else {
                if let phone = currentContact?.phone?.allObjects[indexPath.row] as? Phone,
                    let phoneNumber = phone.phoneNumber {
                    cell.textLabel?.text = ContactsManager.shared.format(phone: phoneNumber)
                    cell.textLabel?.textColor = .black
                    cell.accessoryType = .disclosureIndicator

                    if showEditOptions {
                        cell.accessoryType = .none
                        cell.selectionStyle = .none
                    }
                }
            }
        } else if indexPath.section == 3 { // Email Address section
            if isLastRow, showEditOptions {
                cell.textLabel?.text = "Add Email Address"
                cell.textLabel?.textColor = actionColor
            } else {
                if let email = currentContact?.email?.allObjects[indexPath.row] as? Email {
                    cell.textLabel?.text = email.emailAddress
                    cell.textLabel?.textColor = .black
                    cell.accessoryType = .disclosureIndicator
                    
                    if showEditOptions {
                        cell.accessoryType = .none
                        cell.selectionStyle = .none
                    }
                }
            }
        } else if indexPath.section == 4 { //Address section
            if isLastRow, showEditOptions {
                cell.textLabel?.text = "Add Address"
                cell.textLabel?.textColor = actionColor
            } else {
                if let address = currentContact?.address?.allObjects[indexPath.row] as? Address {
                    cell.textLabel?.text = address.displayName
                    cell.textLabel?.textColor = .black
                    cell.accessoryType = .disclosureIndicator
                    cell.textLabel?.numberOfLines = 0
                    
                    if showEditOptions {
                        cell.accessoryType = .none
                        cell.selectionStyle = .none
                    }
                }
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //check if row is last in its section
        let isLastRow = Bool(indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1)
        
        if let currentContact = self.currentContact, !showEditOptions {
            if indexPath.section == 0 {
                
            } else if indexPath.section == 1 {
                if indexPath.row == 0 {
                    
                } else if indexPath.row == 1 {
                    if let birthday = currentContact.dateOfBirth {
                        let store = EKEventStore()
                        store.requestAccess(to: .event) { (success, err) in
                            
                            DispatchQueue.main.async {
                                if success {
                                    let newEvent = EKEvent(eventStore: store)
                                    newEvent.title = "\(currentContact.fullName)'s Birthday"
                                    newEvent.startDate = birthday
                                    newEvent.endDate = birthday
                                    newEvent.isAllDay = true
                                    newEvent.recurrenceRules = [EKRecurrenceRule(recurrenceWith: EKRecurrenceFrequency.yearly, interval: 1, end: nil)]
                                    newEvent.calendar = store.defaultCalendarForNewEvents
                                    do {
                                        try store.save(newEvent, span: .thisEvent)
                                        
                                        if let calendarURL = URL(string: "calshow:\(birthday.timeIntervalSinceReferenceDate)"),
                                            UIApplication.shared.canOpenURL(calendarURL) {
                                            UIApplication.shared.open(calendarURL, options: [:], completionHandler: nil)
                                        }
                                    } catch {
                                        self.showStatusMessage(title: "Error Adding Event", message: "Unable to add \(currentContact.fullName)'s birthday to your calendar.")
                                    }
                                    
                                } else {
                                    let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { (action) in
                                        if let url = URL(string: UIApplication.openSettingsURLString) {
                                            if UIApplication.shared.canOpenURL(url) {
                                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                            }
                                        }
                                    })
                                    
                                    self.showStatusMessage(title: "Calendar Access Denied", message: "You must enable calendar access in the Settings app.", additionalActions: [settingsAction])
                                }
                            }
                            
                        }
                    }
                    
                }
            } else if indexPath.section == 2 {
                if let phone = currentContact.phone?.allObjects[indexPath.row] as? Phone {
                    self.showOptions(forType: .phone, value: phone.phoneNumber)
                }
            } else if indexPath.section == 3 {
                if let email = currentContact.email?.allObjects[indexPath.row] as? Email {
                    self.showOptions(forType: .email, value: email.emailAddress)
                }
            } else if indexPath.section == 4 {
                if let address = currentContact.address?.allObjects[indexPath.row] as? Address {
                    self.showOptions(forType: .address, value: address.formattedAddressList)
                }
            }
        } else if showEditOptions {
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    self.showEditor(forType: .firstName, defaultValue: self.currentContact?.firstName) { (newValue) in
                        self.currentContact?.firstName = newValue
                        self.tableView.reloadData()
                    }
                } else if indexPath.row == 1 {
                    self.showEditor(forType: .lastName, defaultValue: self.currentContact?.lastName) { (newValue) in
                        self.currentContact?.lastName = newValue
                        self.tableView.reloadData()
                    }
                }
            } else if indexPath.section == 1 {
                if indexPath.row == 0 {
                    presentDatePicker()
                }
            } else if indexPath.section == 2 {
                if isLastRow {
                    self.showEditor(forType: .phone, defaultValue: nil) { (newValue) in
                        if ContactsManager.shared.validate(phone: newValue) {
                            let newPhone = Phone(context: ContactsManager.shared.context)
                            newPhone.person = self.currentContact
                            newPhone.phoneNumber = newValue
                            self.currentContact?.addToPhone(newPhone)
                            self.tableView.reloadData()
                        } else {
                            self.showStatusMessage(title: "Invalid Phone Number", message: "The phone number you entered is not valid.")
                        }
                    }
                }
            } else if indexPath.section == 3 {
                if isLastRow {
                    self.showEditor(forType: .email, defaultValue: nil) { (newValue) in
                        if ContactsManager.shared.validate(email: newValue) {
                            let newEmail = Email(context: ContactsManager.shared.context)
                            newEmail.person = self.currentContact
                            newEmail.emailAddress = newValue
                            self.currentContact?.addToEmail(newEmail)
                            self.tableView.reloadData()
                        } else {
                            self.showStatusMessage(title: "Invalid Email Address", message: "The email address you entered is not valid.")
                        }
                    }
                }
            } else if indexPath.section == 4 {
                if isLastRow {
                    performSegue(withIdentifier: "EditAddress", sender: nil)
                } else {
                    performSegue(withIdentifier: "EditAddress", sender: indexPath)
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        if !showEditOptions, (indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4)  {
            return true
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        let isCopyAction = action == #selector(copy(_:))
        return isCopyAction
    }
    
    override func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        let board = UIPasteboard.general
        
        if indexPath.section == 0 {
            board.string = self.currentContact?.fullName
        } else if indexPath.section == 2 {
            board.string = (self.currentContact?.phone?.allObjects[indexPath.row] as? Phone)?.phoneNumber
        } else if indexPath.section == 3 {
            board.string = (self.currentContact?.email?.allObjects[indexPath.row] as? Email)?.emailAddress
        } else if indexPath.section == 4 {
            board.string = (self.currentContact?.address?.allObjects[indexPath.row] as? Address)?.displayName
        }
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        //check if row is last in its section
        let isLastRow = Bool(indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1)
        
        if showEditOptions {
            if (indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4), !isLastRow  {
                return true
            }
        }
        
        return false
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        //only support deleting of NSSet values
        if editingStyle == .delete {
            if indexPath.section == 2 {
                if let phone = currentContact?.phone?.allObjects[indexPath.row] as? Phone {
                    currentContact?.removeFromPhone(phone)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
            } else if indexPath.section == 3 {
                if let email = currentContact?.email?.allObjects[indexPath.row] as? Email {
                    currentContact?.removeFromEmail(email)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
            } else if indexPath.section == 4 {
                if let address = currentContact?.address?.allObjects[indexPath.row] as? Address {
                    currentContact?.removeFromAddress(address)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
            }
            
            //If we aren't in the editor, and the user wants and entry deleted, save the Person immediately
            if !showEditOptions {
                try? ContactsManager.shared.context.save()
            }
            
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "EditContact",
            let navVC = segue.destination as? UINavigationController,
            let detailVC = navVC.viewControllers[0] as? ContactDetailViewController {
            detailVC.isEditingContact = true
            detailVC.currentContact = self.currentContact
        } else if segue.identifier == "EditAddress",
            let navVC = segue.destination as? UINavigationController,
            let detailVC = navVC.viewControllers[0] as? AddressEditorViewController {
            
            detailVC.delegate = self
            
            if let sender = sender as? IndexPath {
                detailVC.currentAddress = self.currentContact?.address?.allObjects[sender.row] as? Address
            }
        }
    }
}

// MARK: - Date Picker
extension ContactDetailViewController {
    
    func presentDatePicker() {
        
        if let birthday = currentContact?.dateOfBirth {
            datePicker.date = birthday
        }
        
        let inputToolbar = UIToolbar()
        
        let selectDateButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(selectDate))
        let flexButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelDateButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        inputToolbar.setItems([cancelDateButton, flexButton, selectDateButton], animated: false)
        
        inputToolbar.sizeToFit()

        datePickerTextField.inputAccessoryView = inputToolbar
        datePickerTextField.inputView = datePicker
        self.view.addSubview(datePickerTextField)
        
        datePickerTextField.becomeFirstResponder()
    }
    
    @objc func selectDate() {
        
        currentContact?.dateOfBirth = datePicker.date
        
        cancelDatePicker()
        
        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .fade)
    }
    
    @objc func cancelDatePicker() {
        datePickerTextField.resignFirstResponder()
        datePickerTextField.removeFromSuperview()
    }
}

// MARK: - Address Editor delegate methods

extension ContactDetailViewController: AddressEditorViewControllerDelegate {
    func didSave(address: Address, editor: AddressEditorViewController) {
        if address.objectID.isTemporaryID {
            self.currentContact?.addToAddress(address)
        }
        
        self.tableView.reloadData()
        
        editor.dismiss(animated: true, completion: nil)
    }
    
    func didCancel(editor: AddressEditorViewController) {
        editor.dismiss(animated: true, completion: nil)
    }
}
