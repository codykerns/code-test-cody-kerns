//
//  AddressEditorViewController.swift
//  Code Test Cody Kerns
//
//  Created by Cody Kerns on 12/19/18.
//  Copyright © 2018 Cody Kerns. All rights reserved.
//

import UIKit

protocol AddressEditorViewControllerDelegate {
    func didSave(address: Address, editor: AddressEditorViewController)
    func didCancel(editor: AddressEditorViewController)
}

class AddressEditorViewController: UIViewController {

    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var stateField: UITextField!
    @IBOutlet weak var zipField: UITextField!
    @IBOutlet weak var countryField: UITextField!

    public var delegate: AddressEditorViewControllerDelegate?
    
    public var currentAddress: Address?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Edit Address"
        
        self.view.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.00)

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelEditor))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveAddress))
        
        if currentAddress == nil {
            currentAddress = Address(context: ContactsManager.shared.context)
        }
        
        for field in [addressField, cityField, stateField, zipField, countryField] {
            field?.delegate = self
            field?.smartInsertDeleteType = .yes
            field?.autocapitalizationType = .words
        }
        
        addressField.text = currentAddress?.street
        cityField.text = currentAddress?.city
        stateField.text = currentAddress?.state
        zipField.text = currentAddress?.zip
        countryField.text = currentAddress?.country
        
        addressField.becomeFirstResponder()
    }
    
    @objc func saveAddress() {
        guard let address = currentAddress else {
            return
        }
        
        address.street = addressField.text?.trimmingCharacters(in: .whitespaces)
        address.city = cityField.text?.trimmingCharacters(in: .whitespaces)
        address.state = stateField.text?.trimmingCharacters(in: .whitespaces)
        address.zip = zipField.text?.trimmingCharacters(in: .whitespaces)
        address.country = countryField.text?.trimmingCharacters(in: .whitespaces)
        
        self.delegate?.didSave(address: address, editor: self)
    }
    
    @objc func cancelEditor() {
        self.delegate?.didCancel(editor: self)
    }
}

extension AddressEditorViewController: UITextFieldDelegate {
    
}
