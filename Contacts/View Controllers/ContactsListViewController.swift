//
//  ContactsListViewController.swift
//  Code Test Cody Kerns
//
//  Created by Cody Kerns on 12/17/18.
//  Copyright © 2018 Cody Kerns. All rights reserved.
//

import UIKit
import Contacts

class ContactsListViewController: UITableViewController {

    //maintain reference to the manager
    var manager = ContactsManager.shared
    
    //create a search controller
    let searchController = UISearchController(searchResultsController: nil)
    
    //keep track of search status
    var isSearching: Bool = false
    var searchResults: [Person] = []
    
    //cached sorted contacts
    var cachedContacts: [String : [Person]] = [:]
    var sortedSectionTitles: [String] {
        let sections = Array(cachedContacts.keys)
        return sections.sorted(by: { $0 == "★" ? true : $0 < $1 })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup search controller, ensure proper tint, define presentation context
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.tintColor = self.navigationController?.navigationBar.tintColor
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
        
        //setup navigation items
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createNewContact))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Options", style: .plain, target: self, action: #selector(showAppOptions))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //refresh contacts
        sortContacts()
    }
    
    func getContact(byUUID uuid: String) {
        if let contact = manager.searchDatabase(type: Person.self, query: uuid).first {
            performSegue(withIdentifier: "PresentContactDetails", sender: contact)
        }
    }
    
    func sortContacts() {
        let contacts = manager.myContacts()
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let weakSelf = self else {
                return
            }
            
            //clear previous data
            weakSelf.cachedContacts = [:]
            
            //sort contacts into rough-alphabetical sections
            for contact in contacts {
                let sectionTitle = String(contact.lastName!.prefix(1)).uppercased()
                if var sectionList = weakSelf.cachedContacts[sectionTitle] {
                    sectionList.append(contact)
                    
                    weakSelf.cachedContacts[sectionTitle] = sectionList.sorted(by: { $0.lastName ?? "" < $1.lastName ?? "" })
                } else {
                    let newValue = [contact]
                    weakSelf.cachedContacts[sectionTitle] = newValue
                }
                
                if contact.isFavorite == true {
                    if var sectionList = weakSelf.cachedContacts["★"] {
                        sectionList.append(contact)
                        
                        weakSelf.cachedContacts["★"] = sectionList.sorted(by: { $0.lastName ?? "" < $1.lastName ?? "" })
                    } else {
                        let newValue = [contact]
                        weakSelf.cachedContacts["★"] = newValue
                    }
                }
            }
            
            DispatchQueue.main.async {
                weakSelf.tableView.reloadData()
            }
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "PresentContactDetails",
            let detailVC = segue.destination as? ContactDetailViewController {
            
            if let indexPath = sender as? IndexPath {
                //invoked from tableView cell selection
                //get the cell's Person object, pass it on
                if isSearching {
                    detailVC.currentContact = self.searchResults[indexPath.row]
                } else {
                    let sectionTitle = sortedSectionTitles[indexPath.section]
                    if let cachedContact = cachedContacts[sectionTitle]?[indexPath.row] {
                        detailVC.currentContact = cachedContact
                    }
                }
            } else if let contact = sender as? Person {
                detailVC.currentContact = contact
            }
            
        } else if segue.identifier == "CreateNewContact",
            let navVC = segue.destination as? UINavigationController,
            let detailVC = navVC.viewControllers[0] as? ContactDetailViewController {
            detailVC.isNewContact = true
        }
        
    }

}

// Data source extension methods
extension ContactsListViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching {
            return 1
        }
        return cachedContacts.keys.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return searchResults.count
        }
        
        let sectionTitle = sortedSectionTitles[section]
        if let contacts = cachedContacts[sectionTitle] {
            return contacts.count
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearching {
            return nil
        }
        
        var sectionTitle = sortedSectionTitles[section]
        if sectionTitle == "★" {
            sectionTitle = sectionTitle + " Favorites"
        }
        return sectionTitle
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isSearching {
            return nil
        }
        
        return sortedSectionTitles
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath)
        
        var contact: Person
        if isSearching {
            contact = self.searchResults[indexPath.row]
        } else {
            let sectionTitle = sortedSectionTitles[indexPath.section]
            guard let cachedContact = cachedContacts[sectionTitle]?[indexPath.row] else {
                return cell
            }
            
            contact = cachedContact
        }
        
        //set cell data
        let attributedName = NSMutableAttributedString(string: contact.fullName)
        
        if let lastName = contact.lastName {
            attributedName.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17.0, weight: .bold), range: NSMakeRange(contact.fullName.count - lastName.count, lastName.count))
        }
        
        cell.textLabel?.attributedText = attributedName
        
        return cell
    }
}

// Delegate extension methods
extension ContactsListViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PresentContactDetails", sender: indexPath)
    }
}

extension ContactsListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        self.isSearching = searchController.isActive
        if let query = searchController.searchBar.text {
            if query.count == 0 {
                self.searchResults = []
            } else {
                let results = ContactsManager.shared.searchDatabase(type: Person.self, query: query)
                self.searchResults = results
            }
        }
        self.tableView.reloadData()
    }
}


// Import previous contacts
extension ContactsListViewController {
    
    //NOTE: This will ONLY import contacts that meet the requirements:
    // 1. First Name
    // 2. Last Name
    // 3. Birthday
    // 4. One or more email addresses
    // 5. One or more phone numbers
    // 6. Zero or more postal addresses
    public func attemptImportRequest() {
        let controller = UIAlertController(title: "Import contacts?", message: "Preload your contacts from your device.", preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        controller.addAction(UIAlertAction(title: "Import", style: .default, handler: { (action) in
            let contactStore = CNContactStore()
            
            contactStore.requestAccess(for: .contacts, completionHandler: { (success, error) in
                guard success == true else {
                    let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { (action) in
                        if let url = URL(string: UIApplication.openSettingsURLString) {
                            if UIApplication.shared.canOpenURL(url) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            }
                        }
                    })
                    
                    self.showStatusMessage(title: "Contacts Access Denied", message: "You must enable access to your contacts in the Settings app.", additionalActions: [settingsAction])
                    return
                }
                
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactBirthdayKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactPostalAddressesKey] as [CNKeyDescriptor]
                let request = CNContactFetchRequest(keysToFetch: keys)
                
                var importedContacts: Int = 0
                
                do {
                    let context = ContactsManager.shared.context
                    
                    try contactStore.enumerateContacts(with: request) { (contact, stop) in
                        let person = Person(context: context)
                        person.firstName = contact.givenName
                        person.lastName = contact.familyName
                        person.dateOfBirth = contact.birthday?.date
                        
                        for address in contact.postalAddresses {
                            let newAddress = Address(context: context)
                            newAddress.street = address.value.street
                            newAddress.city = address.value.city
                            newAddress.state = address.value.state
                            newAddress.zip = address.value.postalCode
                            newAddress.country = address.value.country
                            newAddress.person = person
                            person.addToAddress(newAddress)
                        }
                        
                        for email in contact.emailAddresses {
                            let newEmail = Email(context: context)
                            newEmail.emailAddress = String(email.value)
                            person.addToEmail(newEmail)
                        }
                        
                        for phone in contact.phoneNumbers {
                            let newPhone = Phone(context: context)
                            newPhone.phoneNumber = phone.value.stringValue
                            person.addToPhone(newPhone)
                        }
                        
                        //Attempt to save new contact. If it fails, wipe is from the context
                        do {
                            try context.save()
                            
                            importedContacts = importedContacts + 1
                        } catch {
                            context.rollback()
                        }
                    }
                    
                    //When complete, refresh contacts list
                    DispatchQueue.main.async {
                        
                        self.showStatusMessage(title: "Finished Import", message: "\(importedContacts) contacts successfully imported.")
                        self.sortContacts()
                    }
                    
                } catch {
                    
                }
                
            })
        }))
        self.present(controller, animated: true, completion: nil)
    }
    
}

// Navigation item actions
extension ContactsListViewController {
    @objc func showAppOptions() {
        let optionsAlert = UIAlertController(title: "Options", message: nil, preferredStyle: .actionSheet)
        optionsAlert.addAction(UIAlertAction(title: "About", style: .default, handler: { (_) in
            let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
            let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
            
            self.showStatusMessage(title: "About", message: "Contacts \(version) (\(build))\n\nCode Test Cody Kerns\n\n© Cody Kerns 2018")
        }))
        optionsAlert.addAction(UIAlertAction(title: "Import Contacts", style: .default, handler: { (_) in
            self.attemptImportRequest()
        }))
        optionsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        optionsAlert.popoverPresentationController?.barButtonItem = self.navigationItem.leftBarButtonItem
        self.present(optionsAlert, animated: true, completion: nil)
    }
    
    @objc func createNewContact() {
        performSegue(withIdentifier: "CreateNewContact", sender: nil)
    }
}
