//
//  Person.swift
//  Code Test Cody Kerns
//
//  Created by Cody Kerns on 12/17/18.
//  Copyright © 2018 Cody Kerns. All rights reserved.
//

import UIKit
import CoreData
import CoreSpotlight
import MobileCoreServices

extension Person {
    var fullName: String {
        var bothNames = false
        if (self.firstName != nil) && (self.lastName != nil) {
            bothNames = true
        }
        let projectedName = (self.firstName ?? "") + (bothNames ? " " : "") + (self.lastName ?? "")
        
        return projectedName.count > 0 ? projectedName : "Unnamed"
    }
    
    func removeFromSpotlight(completion: ((Error?) -> Void)?) {
        guard let id = self.uniqueId else {
            return
        }
        
        CSSearchableIndex.default().deleteSearchableItems(withIdentifiers: [id], completionHandler: completion)
    }
    
    // implement custom id value for each Person object, index contacts in Spotlight
    public override func willSave() {
        if self.uniqueId == nil {
            self.uniqueId = UUID().uuidString
        }
        
        super.willSave()
        
        guard CSSearchableIndex.isIndexingAvailable() else {
            return
        }
        
        self.removeFromSpotlight { (error) in
            
            let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeData as String)
            attributeSet.title = self.fullName
            attributeSet.identifier = self.uniqueId
            let newItem = CSSearchableItem(uniqueIdentifier: self.uniqueId, domainIdentifier: "com.codykerns.Contacts.Person", attributeSet: attributeSet)
            CSSearchableIndex.default().indexSearchableItems([newItem], completionHandler: nil)
        }
    }
}

extension Address {
    
    var addressComponents: [String] {
        var components: [String] = []
        
        if let street = self.street, street.count > 0 {
            components.append(street)
        }
        
        if let city = self.city, city.count > 0 { //attempt to format city, state, zip together
            if let state = self.state, state.count > 0 {
                if let zip = self.zip, zip.count > 0 {
                    components.append("\(city), \(state) \(zip)")
                } else {
                    components.append("\(city), \(state)")
                }
                
            } else { // If there's no state, attempt to format city, zip together
                if let zip = self.zip, zip.count > 0 {
                    components.append("\(city), \(zip)")
                } else {
                    components.append(city)
                }
                
            }
        } else if let state = self.state, state.count > 0 { //If there's no city, attempt to format state, zip together
            if let zip = self.zip, zip.count > 0 {
                components.append("\(state), \(zip)")
            } else {
                components.append(state)
            }
        } else if let zip = self.zip, zip.count > 0 { //If there's no city or state, just add zip
            components.append(zip)
        }
        
        if let country = self.country, country.count > 0 {
            components.append(country)
        }
        return components
    }
    
    var displayName: String {
        return self.addressComponents.joined(separator: "\n")
    }
    
    var formattedAddressList: String {
        return self.addressComponents.joined(separator: ",").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
}
