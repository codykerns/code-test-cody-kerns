//
//  ContactsManager.swift
//  Code Test Cody Kerns
//
//  Created by Cody Kerns on 12/17/18.
//  Copyright © 2018 Cody Kerns. All rights reserved.
//

import UIKit
import CoreData

class ContactsManager: NSObject {
    static let shared = ContactsManager()
    
    //-------------------------
    //Variables and Declarations

    //var to maintain a reference to the Core Data context
    public var context: NSManagedObjectContext {
        return AppDelegate.shared.persistentContainer.viewContext
    }
    
    //-------------------------
    //CRUD---------------------
    
    public func myContacts() -> [Person] {
        let request = NSFetchRequest<Person>(entityName: "Person")
        return (try? context.fetch(request)) ?? []
    }
    
    //Search any entities by passing T
    public func searchDatabase<T: NSManagedObject>(type: T.Type, query: String) -> [T] {
        let request = NSFetchRequest<T>(entityName: T.entity().name!)
        
        var subPredicates: [NSPredicate] = []
        for property in T.entity().properties {
            if let property = property as? NSAttributeDescription,
                property.attributeType == .stringAttributeType {
                let subPredicate = NSPredicate(format: "\(property.name) CONTAINS[cd] %@", query)
                subPredicates.append(subPredicate)
            }
        }
        request.predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: subPredicates)

        return (try? context.fetch(request)) ?? []
    }
    
    //-------------------------
    //Validation---------------
    
    public enum OptionType: String {
        case firstName = "First Name"
        case lastName = "Last Name"
        case email = "Email Address"
        case phone = "Phone Number"
        case address = "Address"
    }
    
    // email validation
    public func validate(email: String) -> Bool {
        //although not exhaustive of specific domains, this will match the general format of email addresses
        let regex = try? NSRegularExpression(pattern: "^([a-zA-Z1-9]{1,50})@([a-zA-Z1-9]{1,59}\\.)([a-zA-Z1-9]{2,50})$", options: .caseInsensitive)
        let matches = regex?.matches(in: email, options: [], range: NSMakeRange(0, email.count))
        return (matches?.count ?? 0) > 0
    }
    
    // phone validation and formatting
    public func validate(phone: String) -> Bool {
        let regex = try? NSRegularExpression(pattern: "^(\\d{11})|((\\+?)\\d{1}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$|\\d{10}", options: .caseInsensitive)
        let matches = regex?.matches(in: phone, options: [], range: NSMakeRange(0, phone.count))
        return (matches?.count ?? 0) > 0
    }
    
    public func format(phone: String) -> String {
        let strippedPhone = phone.replacingOccurrences(of: "+", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: " ", with: "")
        
        if strippedPhone.count == 11 {
            return "+\(strippedPhone.prefix(1)) (\(strippedPhone.dropFirst().prefix(3))) \(strippedPhone.dropFirst(4).dropLast(4))-\(strippedPhone.dropFirst(7))"
        } else if strippedPhone.count == 10 {
            return "(\(strippedPhone.prefix(3))) \(strippedPhone.dropFirst(3).dropLast(4))-\(strippedPhone.dropFirst(6))"
        }
        
        //if we can't format their number, return their entry
        return phone
    }
    
}

extension UIViewController {
    func showEditor(forType type: ContactsManager.OptionType, defaultValue: String?, saveBlock: ((String) -> Void)?) {
        let editor = UIAlertController(title: "\(defaultValue != nil ? "Edit" : "New") \(type.rawValue)", message: nil, preferredStyle: .alert)
        editor.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        editor.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            let newValue = editor.textFields?[0].text ?? (defaultValue ?? "")
            saveBlock?(newValue.count > 0 ? newValue.trimmingCharacters(in: .whitespaces) : (defaultValue ?? ""))
        }))
        editor.addTextField { (textField) in
            textField.placeholder = type.rawValue
            textField.text = defaultValue
            textField.autocapitalizationType = UITextAutocapitalizationType.words
            
            if type == .phone {
                textField.keyboardType = .phonePad
                textField.textContentType = .telephoneNumber
            } else if type == .email {
                textField.keyboardType = .emailAddress
                textField.textContentType = .emailAddress
            }
        }
        self.present(editor, animated: true, completion: nil)
    }
    
    func showOptions(forType type: ContactsManager.OptionType, value: String?) {
        let editor = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        editor.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if type == .phone {
            editor.title = value
            
            editor.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
                if let phoneURL = URL(string: "tel://\(value?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")") {
                    if UIApplication.shared.canOpenURL(phoneURL) {
                        UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
                    }
                }
            }))
            editor.addAction(UIAlertAction(title: "Send Message", style: .default, handler: { (action) in
                if let phoneURL = URL(string: "sms://\(value?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")") {
                    if UIApplication.shared.canOpenURL(phoneURL) {
                        UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
                    }
                }
            }))
        } else if type == .email {
            editor.title = value

            editor.addAction(UIAlertAction(title: "Send Email", style: .default, handler: { (action) in
                if let mailto = URL(string: "mailto://\(value ?? "")") {
                    if UIApplication.shared.canOpenURL(mailto) {
                        UIApplication.shared.open(mailto, options: [:], completionHandler: nil)
                    }
                }
            }))
        } else if type == .address {
            editor.addAction(UIAlertAction(title: "Open in Maps", style: .default, handler: { (action) in
                if let maps = URL(string: "http://maps.apple.com/?address=\(value ?? "")") {
                    if UIApplication.shared.canOpenURL(maps) {
                        UIApplication.shared.open(maps, options: [:], completionHandler: nil)
                    }
                }
            }))
        }
        
        
        self.present(editor, animated: true, completion: nil)
    }
    
    func showStatusMessage(title: String?, message: String?, additionalActions: [UIAlertAction]? = nil) {
        let error = UIAlertController(title: title, message:message, preferredStyle: .alert)
        error.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        if let additionalActions = additionalActions {
            for action in additionalActions {
                error.addAction(action)
            }
        }
        
        self.present(error, animated: true, completion: nil)
        
        UINotificationFeedbackGenerator().notificationOccurred(.warning)
    }
}
